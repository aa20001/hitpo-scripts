// ==UserScript==
// @name         moodle extender
// @namespace    http://aa20001.gitlab.io/hitpo-scripts
// @updateURL    https://gitlab.com/aa20001/hitpo-scripts/-/raw/main/moodle%20extender.user.js
// @version      0.4
// @description  moodleのスクロール量を減らします
// @author       AA20001
// @include      /https?:\/\/pfmdl[0-9]{2}.it-hiroshima.ac.jp\/moodle[0-9]{4}.*$/
// @icon         https://www.google.com/s2/favicons?domain=it-hiroshima.ac.jp
// @require      https://openuserjs.org/src/libs/sizzle/GM_config.min.js
// @grant        GM_registerMenuCommand
// @grant        GM_unregisterMenuCommand
// @grant        GM_setValue
// @grant        GM_getValue

// ==/UserScript==

const GLOBAL_SAVE_DEFAULT = "sessionStorage";
const SESSION_SAVE_DEFAULT = "global";


(function() {
    'use strict';

    const getPageUnique = () => {
        return window.location.pathname + window.location.search
    }
    const storageConfigPrefix = "__GM_MoodleExtender_Config_";

    const getPageSaveStorage = () => {
        // セッションストレージに設定があったらそっちを優先。そうでないならグローバル設定を適用
        const storageType = sessionStorage.getItem(storageConfigPrefix + "saveStorage") || GM_getValue("globalSaveStorage") || GLOBAL_SAVE_DEFAULT;
        switch (storageType) {
            case "localStorage":
                return localStorage
            case "sessionStorage":
                return sessionStorage
            default:
                return {getItem: () => null, setItem: () => {}, removeItem: () => {}, clear: () => {}}
        }
    }

    const menuInit = () => {
        GM_config.init({
            id: "Config",
            title: "MoodleExtender Configuration",
            fields: {
                "globalSaveStorage": {
                    label: "globalSaveStorage",
                    type: "select",
                    default: "none",
                    value: GM_getValue("globalSaveStorage") || GLOBAL_SAVE_DEFAULT,
                    options: ["none","localStorage", "sessionStorage"]
                },
                "tabSiteSaveStorage": {
                    label: "tabSiteSaveStorage",
                    type: "select",
                    default: "global",
                    value: sessionStorage.getItem(storageConfigPrefix + "saveStorage") || SESSION_SAVE_DEFAULT,
                    options: ["none", "global","localStorage", "sessionStorage"]
                }
            },
            events: {
                save: (values) => {
                    GM_setValue("globalSaveStorage", GM_config.fields["globalSaveStorage"].value);

                    if (GM_config.fields["tabSiteSaveStorage"].value !== "global") {
                        sessionStorage.setItem(storageConfigPrefix + "saveStorage", GM_config.fields["tabSiteSaveStorage"].value);
                    } else {
                        sessionStorage.removeItem(storageConfigPrefix + "saveStorage", GM_config.fields["tabSiteSaveStorage"].value);
                    }
                    console.log("write config");

                    GM_config.close();
                }
            }
        });
        GM_config.fields["globalSaveStorage"].value = GM_getValue("globalSaveStorage") || GLOBAL_SAVE_DEFAULT;
        GM_config.fields["tabSiteSaveStorage"].value = sessionStorage.getItem(storageConfigPrefix + "saveStorage") || SESSION_SAVE_DEFAULT;
    }

    const storageStatePrefix = "__GM_MoodleExtender_State_";

    //params state: is_open
    const savePageState = (id, state) => {
        const storage = getPageSaveStorage();
        const states = JSON.parse(storage.getItem(storageStatePrefix + getPageUnique()) || "{}");
        states[id] = state;
        storage.setItem(storageStatePrefix + getPageUnique(), JSON.stringify(states))
    }

    const getPageState = (id) => {
        const storage = getPageSaveStorage();
        const states = JSON.parse(storage.getItem(storageStatePrefix + getPageUnique()) || "{}");
        return states.hasOwnProperty(id)?states[id]:false
    }

    const changeTagName = (old, tagName) => {
        const _new = document.createElement(tagName);
        for (const attr of Array.from(old.attributes).values()) _new.setAttribute(attr.name, attr.value);
        for (const elem of Array.from(old.children).values()) _new.appendChild(elem);
        return _new;
    }


    /**
    *** 教員リスト等の部分を隠します
    **/
    // 講義一覧ページで実行
    if (window.location.pathname.match("^/moodle[0-9]{4}\/?$")) {
        const toggleall = document.createElement("a");
        toggleall.textContent = "全て開く";
        toggleall.style.margin = "2px";
        toggleall.onclick = (event) => {
            const new_open = !Array.from(document.querySelectorAll("details.coursebox")).every((item) => item.open)
            if (!event) toggleall.textContent = 　new_open?"全て開く":"全て閉じる";
            if (event && event.target === toggleall) {
                for (const elem of document.querySelectorAll("details.coursebox")) if (elem.open !== new_open) elem.click();
                toggleall.onclick();
            }


        }

        document.querySelector(".paging").appendChild(toggleall)
        for (const coursebox of document.querySelectorAll(".coursebox")) {
            const info = coursebox.querySelector(".info");
            const __new = changeTagName(info, "summary");
            __new.style.display = "block";
            __new.onclick = (event) => { if (event.target === __new) event.preventDefault(); }
            info.parentElement.replaceChild(__new, info);

            const _new = changeTagName(coursebox, "details");
            _new.open = getPageState(_new.dataset.courseid);
            _new.onclick = (event) => {
                if (event.target === _new) {
                    _new.open = !_new.open;
                    savePageState(event.target.dataset.courseid, _new.open);
                    toggleall.onclick();
                }
            }
            coursebox.parentElement.replaceChild(_new, coursebox);
        }

        const style = document.createElement("style");
        style.type = "text/css";
        style.textContent = `
    details.coursebox > summary::-webkit-details-marker {
      display: none;
    }

    `;
        document.body.appendChild(style);

    }

    // 講義ページで実行
    if (document.querySelector('a[title="ポートフォリオサイト"]') && document.querySelector('a[title="ポートフォリオサイト"]').href.includes("idnumber=")) {
         const toggleall = document.createElement("a");
        toggleall.textContent = "全て開く";
        toggleall.style.margin = "2px";
        toggleall.onclick = (event) => {
            const new_open = !Array.from(document.querySelectorAll(".section > details")).every((item) => item.open);
            if (!event) toggleall.textContent = 　new_open?"全て開く":"全て閉じる";
            if (event && event.target === toggleall) {
                for (const elem of document.querySelectorAll(".section > details.content > summary")) if (elem.parentElement.open !== new_open) elem.click();
                toggleall.onclick();
            }
        }
        const div = document.createElement("div");
        div.classList.add("paging", "paging-morelink");
        div.style.textAlign = "center";
        div.appendChild(toggleall);
        document.querySelector(".course-content").appendChild(div)
        for (const content of document.querySelectorAll(".section > .content")) {
            if (content.parentElement.id === "section-0") {continue};

            const sectionname = content.querySelector(".sectionname");
            if (!sectionname) return;
            const __new = document.createElement("summary");
            __new.innerHTML = sectionname.outerHTML;
            //const _new = changeTagName(sectionname, "summary");
            __new.style.display = "block";

            sectionname.parentElement.replaceChild(__new, sectionname);

            const _new = changeTagName(content, "details");
            const section_id = content.parentElement.id
            _new.open = getPageState(section_id);
            __new.onclick = (event) => {
                _new.open = !_new.open;
                console.log(section_id);
                savePageState(section_id, _new.open);
                toggleall.onclick();
                event.preventDefault();
            }
            content.parentElement.replaceChild(_new, content);

        }
        const style = document.createElement("style");
        style.type = "text/css";
        style.textContent = `
    .section > details.content > summary::-webkit-details-marker {
      display: none;
    }

    .section > details.content > summary > .sectionname {
      margin-bottom: 5px;
    }
    .section > details.content > summary > .sectionname > span:before {
        content: "▼";
        display: inline-block;
        margin-right: 10px;
        transform: rotate(-90deg);
    }
    .section > details.content[open] > summary > .sectionname > span:before {
        transform: rotate(0deg);
    }

    `;
        document.body.appendChild(style);
    }
    // 以上

    // メニュー登録
    const openmenu = () => GM_config.open();
    menuInit();
    window.addEventListener("focus", () => {
        menuInit()
        GM_unregisterMenuCommand("Config");
        GM_registerMenuCommand("Config", openmenu);
    });
    GM_registerMenuCommand("Config", openmenu);
})();
