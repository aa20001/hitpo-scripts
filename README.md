# Hitpo Scripts

hitpo用のユーザースクリプト集

## Objeactive
* moodleからシラバスを開く際の手間の軽減
* moodleのスクロール量の軽減及びブロックの境界をわかりやすくする

## Provides
* `open syllabus.user.js`  
    moodleのコースのメニューバーにシラバスを開くメニューを追加します  
* `moodle extender.user.js`  
    moodleのコースでの初期状態での視覚的情報量を減らします。
* `copyLectureIds.user.js`
    HITPOのMy時間割に表示されている授業の授業コードをカンマ区切り形式でコピーするボタンを追加します。
    [HITPO Notifier](https://aa20001.x0.com/utils/hitpo_notifier)用のスクリプトです。
* `lecture registration modifier.user.js`
    履修申請画面のカレンダーを前期・後期・およびオリジナルの状態に切り替える機能を提供します。
    

## Warning
特に`moodle extender.user.js`は既存のDOMを変更します。  
その関係上一部jsでの動作がおかしくなる可能性があります。その場合はユーザースクリプトを無効化してください。

## Getting started

1. install userscript extension
  * greasemonkey  
  * tampermonkey  
  ...
2. install userscripts  
    open below urls  
    * [open syllabus](https://gitlab.com/aa20001/hitpo-scripts/-/raw/main/open%20syllabus.user.js)
    * [moodle extender](https://gitlab.com/aa20001/hitpo-scripts/-/raw/main/moodle%20extender.user.js)
    * [copyLectureIds](https://gitlab.com/aa20001/hitpo-scripts/-/raw/main/copyLectureIds.user.js)
    * [lecture registration modifier](https://gitlab.com/aa20001/hitpo-scripts/-/raw/main/lecture%20registration%20modifier.user.js)

## Result

<img src="https://gitlab.com/aa20001/hitpo-scripts/-/raw/main/images/open%20syllabus.gif" />  
<img src="https://gitlab.com/aa20001/hitpo-scripts/-/raw/main/images/moodle%20extender%20course.gif" />  
<img src="https://gitlab.com/aa20001/hitpo-scripts/-/raw/main/images/moodle%20extender%20courses.gif" />  

