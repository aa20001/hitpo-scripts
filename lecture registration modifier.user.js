// ==UserScript==
// @name         lecture registration modifier
// @namespace    http://aa20001.gitlab.io/hitpo-scripts
// @updateURL    https://gitlab.com/aa20001/hitpo-scripts/-/raw/main/lecture%20registration%20modifier.user.js
// @version      0.2
// @description  履修申請画面のカレンダー部分を前期・後期および元の状態で切り替える機能を提供します。
// @author       AA20001
// @include /^https?://sv-hit-gak03.it-hiroshima.ac.jp/uniasv2/.*$/
// @icon         https://www.google.com/s2/favicons?domain=it-hiroshima.ac.jp
// @grant        none
// ==/UserScript==

(function () {
    'use strict';
    if (document.getElementById("idhelp") && document.getElementById("idhelp").textContent == "(ARD010PCT01)") {


        // 履修申請   申請状況画面で実行
        let table = null
        document.querySelectorAll("div.tablearea").forEach(tableArea => {
            let nextElemIsRegistrationTable = false
            for (const tableAreaChildNone of tableArea.childNodes) {
                if (tableAreaChildNone.nodeType === Node.COMMENT_NODE) {
                    if (tableAreaChildNone.textContent.trim() === "履修科目 (テーブル中)") {
                        nextElemIsRegistrationTable = true
                    }
                }
                if (nextElemIsRegistrationTable && tableAreaChildNone instanceof HTMLTableElement) {
                    table = tableAreaChildNone
                    nextElemIsRegistrationTable = false;
                    break

                }
            }
        })
        if (!(table instanceof HTMLTableElement)) {
            return
        }
        const tbody = table.querySelector("tbody");

        if (!(tbody instanceof HTMLTableSectionElement)) {
            return
        }

        const ShowMode = {
            all: 0,
            zenki_tsunen: 1,
            kouki: 2
        }

        function setShowMode(mode) {
            const heads = tbody.querySelectorAll("tr")[0].querySelectorAll("th,td")
            if (mode === ShowMode.all) {
                heads[1].style.display = ""
            } else {
                heads[1].style.display = "none"
            }

            for (const [rowIdx, tr] of Array.from(tbody.querySelectorAll("tr")).slice(1).entries()) {
                console.log(tr)

                const te = tr.querySelectorAll("th,td")
                if (mode === ShowMode.all) {
                    console.log("display all")
                    tr.style.display = ""
                    if (rowIdx % 4 === 0) {
                        te[0].style.display = ""
                        te[0].rowSpan = 4
                        te[1].style.display = ""
                    } else if (rowIdx % 4 === 2) {
                        te[0].style.display = "none"
                        te[1].style.display = ""
                    }
                    // } else if (rowIdx % 2 == 0) {
                    //     te[1].style.display = undefined
                    // } else {
                        
                    // }
                } else {
                    console.log("display", mode)
                    if (rowIdx % 2 === 0) {
                        te[0].rowSpan = 2
                        te[0].style.display = ""
                        te[1].style.display = "none"
                    }
                    if (mode == ShowMode.zenki_tsunen && [0, 1].includes(rowIdx % 4)) {
                        tr.style.display = ""
                    } else if (mode == ShowMode.kouki && [2, 3].includes(rowIdx % 4)) {
                        tr.style.display = ""
                    } else {
                        tr.style.display = "none"
                    }
                }


            }
        }

        let thHtml = "";
        for (const [rowIdx, tr] of Array.from(tbody.querySelectorAll("tr")).slice(1).entries()) {
            // 履修登録のテーブルは最初の行を除くと各コマが4行ごと(前期登録,前期登録済,後期登録,後期登録済み)に並ぶ構造となっている
            if (rowIdx % 4 === 0) {
                // 前期登録の行の先頭の列何コマ目かを示すセルがcolspan=4で入っているが、前期後期で分けたいのでcolspan=2とした上で内容をコピーする
                thHtml = tr.querySelectorAll("th")[0].outerHTML
            } else if (rowIdx % 4 === 2) {
                // 後期登録の行にコピーした内容を追加
                const newTH = document.createElement("th")
                tr.prepend(newTH)
                newTH.outerHTML = thHtml;
            }
        }
        const qselect = document.createElement("select")
        const qoptionAll = document.createElement("option")
        qoptionAll.textContent = "前期・前期通年・後期"
        qoptionAll.value = ShowMode.all
        const qoptionZenkiTsunen = document.createElement("option")
        qoptionZenkiTsunen.textContent = "前期・前期通年"
        qoptionZenkiTsunen.selected = "selected"
        qoptionZenkiTsunen.value = ShowMode.zenki_tsunen
        const qoptionKouki = document.createElement("option")
        qoptionKouki.textContent = "後期"
        qoptionKouki.value = ShowMode.kouki
        qselect.append(qoptionAll, qoptionZenkiTsunen, qoptionKouki)
        qselect.onchange = () => {
            setShowMode(parseInt(qselect.value))
        }
        qselect.style.padding = "5px 10px"
        qselect.style.margin = "10px 10px"
        qselect.style.height = "100%"
        qselect.style.fontSize = "15px"
        table.insertAdjacentElement("beforebegin", qselect)
        qselect.onchange()
    }
    // Your code here...
})();
