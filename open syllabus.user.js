// ==UserScript==
// @name         open syllabus
// @namespace    http://aa20001.gitlab.io/hitpo-scripts
// @updateURL    https://gitlab.com/aa20001/hitpo-scripts/-/raw/main/open%20syllabus.user.js
// @version      0.2
// @description  Moodleからシラバスへのマクロ及びシラバス画面のちょっとした拡張機能を提供します
// @author       AA20001
// @include        /^https?://pfmdl[0-9]{2}.it-hiroshima.ac.jp/moodle[0-9]{4}/.*$/
// @include /^https?://sv-hit-gak03.it-hiroshima.ac.jp/uniasv2/.*$/
// @icon         https://www.google.com/s2/favicons?domain=it-hiroshima.ac.jp
// @grant        none
// ==/UserScript==

(function() {
    'use strict';
    const SYLLABUS_URL = "https://sv-hit-gak03.it-hiroshima.ac.jp/uniasv2/UnSSOLoginControlFree?REQ_ACTION_DO=/AGA030.do&REQ_PRFR_FUNC_ID=T_AGA030_1&CRR_FLG=1";
    const SYLLABUS_INPUT_NAME = "txtLsnCd_01";
    const params = new Map((new URLSearchParams(window.location.search.substring(1))));

    // サイトがコース管理の場合に実行
    if (window.location.pathname.includes("moodle")) {
        console.log("CMS");
        const pf_node = document.querySelector('a[title="ポートフォリオサイト"]');
        const pf_url = new URL(pf_node.href);
        const pf_params = new Map((new URLSearchParams(pf_url.search.substring(1))));

        if (pf_params.has("idnumber") && pf_params.get("idnumber") !== '') {
            const idnumber = pf_params.get("idnumber");
            const navbar = pf_node.parentElement.parentElement;

            const a = document.createElement("a");
            a.href = SYLLABUS_URL + "&openWithID=" + idnumber;
            a.title = "SYLLABUS";
            a.target = "_blank";
            const a1 = a.cloneNode();
            a.innerHTML = '<i class="fa fa-list fa-lg" style="margin-right: 1pt"></i>SYLLABUS';
            a.classList.add("nav-link");

            const li = document.createElement("li");
            li.classList.add("nav-item")
            li.appendChild(a);
            navbar.appendChild(li);

            const navbar1 = document.querySelector("#nav-drawer-inner > nav:nth-child(1) > ul")
            a1.innerHTML = '<i class="fa fa-list fa-lg" style="margin-right: 1pt"></i><span class="menutitle">SYLLABUS</span>';
            a1.classList.add("list-group-item");
            a1.classList.add("list-group-item-action");

            navbar1.appendChild(a1);
        }
    //シラバス画面で実行
    } else {
        // 検索画面で実行
        if (document.getElementById("idhelp") && document.getElementById("idhelp").textContent == "(AGA030PSC01)") {
            if (document.querySelector(".errormessage")) return;
            console.log("search");
            const submit_button = document.querySelector('input[name="ESearch"][type="submit"]')
            if ( params.has("preInput") && params.get("preInput") !== "" ) {
                const preInput = new Map(Object.entries(JSON.parse(params.get("preInput"))));
                if (preInput.has("teacher")) document.querySelector('input[name="txtTchKnjfn_01_ST"]').value = preInput.get("teacher")
                document.addEventListener('click', ((timeout_id) => {
                    return (event) => {
                        if (event.target.name !== "ESearch") {
                            clearTimeout(timeout_id);
                        }
                        console.log("clicked");
                    }
                })(setTimeout(() => submit_button.click(), 600)));
            }
            if ( params.has("openWithID") && params.get("openWithID") !== "") {
                const idnumber = params.get("openWithID");
                const form = document.forms[0];
                if (!document.querySelector(`input[name="${SYLLABUS_INPUT_NAME}"]`)) {
                    const input = document.createElement("input");
                    input.name = SYLLABUS_INPUT_NAME;
                    input.style.display = "none";
                    form.appendChild(input)
                }
                const input = document.querySelector(`input[name="${SYLLABUS_INPUT_NAME}"]`);
                input.value = idnumber;

                form.action += "?openWithID=" + idnumber;

                submit_button.click();
            }
        // 検索結果画面で実行
        } else if (document.getElementById("idhelp") && document.getElementById("idhelp").textContent == "(AGA030PLS01)" && params.has("openWithID") && params.get("openWithID") !== "" ) {
            console.log("search results")
            const idnumber = params.get("openWithID");
            const tbody = document.querySelector("table.output > tbody")
            const header = tbody.children[0].children;
            let idx = null;
            // 授業コードのインデックスを検索
            for (const index in header) {
                if (header[index].textContent == "授業コード") {
                    idx = index;
                    break;
                }
            }

            //検索結果ないの授業コードが要求された授業位コードと一致するものを検索
            const results = Array.from(tbody.children).slice(1);
            for (const elem of results) {
                if (elem.children[idx].innerText === idnumber) {
                    const btn = elem.querySelector('input[name="ERefer"');
                    btn.click();
                }
            }
        // 照会画面で実行
        //} else if (window.location.pathname.endsWith("AGA030PLS01EventAction.do") && document.getElementById("idhelp") && document.getElementById("idhelp").textContent == "(AGA030PVI01)") {
        } else if (document.getElementById("idhelp") && document.getElementById("idhelp").textContent == "(AGA030PVI01)") {
            const tchers_node = document.querySelector('input[name="lblTchKnjFn"]');
            const tchers = tchers_node.value.split(",　");
            const field = tchers_node.parentElement;

            for (const elem of field.childNodes) {
                if (elem.nodeType === elem.TEXT_NODE) elem.remove()
            }

            for (const [idx, tcher] of tchers.entries()) {
                if (idx !== 0) field.appendChild(document.createTextNode(", "))
                const a = document.createElement("a");
                a.href = SYLLABUS_URL + "&preInput=" + encodeURIComponent(JSON.stringify({teacher: tcher}));
                a.text = tcher;
                field.appendChild(a);
            }


        }
    }
    // Your code here...
})();
