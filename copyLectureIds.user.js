// ==UserScript==
// @name         copyLectureIds
// @namespace    http://aa20001.gitlab.io/hitpo-scripts
// @version      0.3
// @description  try to take over the world!
// @updateURL    https://gitlab.com/aa20001/hitpo-scripts/-/raw/main/copyLectureIds.user.js
// @author       You
// @match        https://hitpo.it-hiroshima.ac.jp/PfStudent/HitWeb/*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=it-hiroshima.ac.jp
// @grant        none
// ==/UserScript==

(function() {
    'use strict';
    const onCopyLectureIds = (event) => {
        const links = Array.from(document.querySelectorAll(".ari>a"));
        const ids = links.length?links.map(item=>(new URL(item.href)).searchParams.get("lecture_id")).join(","):(new URL(window.location.href)).searchParams.get("lecture_id");
        const textinput = document.createElement("input");
        textinput.type = "text";
        textinput.value = ids;
        document.body.appendChild(textinput);
        textinput.select();
        document.execCommand("copy");
        textinput.remove();
        if (event) event.preventDefault()
    }
    window.degub = onCopyLectureIds;
    const head = document.querySelector(".pageHead");
    const btn = document.createElement("button");
    btn.textContent = "Copy Lecture Ids";
    btn.onclick = onCopyLectureIds;
    btn.type = "button";
    head.appendChild(btn);
    //head.insertBefore(btn, head.querySelector("span"));

    // Your code here...
})();
